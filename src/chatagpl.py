import asyncio
import operator
from typing import Type

import msgpack
import nats
import nats.errors
from maubot import Plugin
from maubot.handlers import event
from mautrix.types import EventType, MessageEvent, ReactionEvent
from mautrix.util.config import BaseProxyConfig, ConfigUpdateHelper


class Config(BaseProxyConfig):
    def do_update(self, helper: ConfigUpdateHelper) -> None:
        helper.copy("nats_server")
        helper.copy("nats_topic_prefix")


class ChatAGPL(Plugin):
    async def start(self) -> None:
        self.config.load_and_update()
        self.log.debug(f'nats_server: {self.config["nats_server"]}')

        self.nats = await nats.connect(self.config['nats_server'])

        self.action_task = asyncio.create_task(self.action_loop())

    @classmethod
    def get_config_class(cls) -> Type[BaseProxyConfig]:
        return Config

    @event.on(EventType.ROOM_MESSAGE)
    async def publish_chat_event(self, evt: MessageEvent) -> None:
        await self.nats.publish(f'{self.config["nats_topic_prefix"]}_message', msgpack.packb({
            'id': evt.event_id,
            'body': evt.content.body,
            'room_id': evt.room_id,
            'formatted_body': evt.content.formatted_body,
            'sender': evt.sender,
            'timestamp': evt.timestamp
        }))

    @event.on(EventType.REACTION)
    async def publish_reaction_event(self, evt: ReactionEvent) -> None:
        emoji = evt.content.relates_to.key
        original_event_id = evt.content.relates_to.event_id

        original_event: MessageEvent = await self.client.get_event(evt.room_id, original_event_id)

        await self.nats.publish(f'{self.config["nats_topic_prefix"]}_reaction', msgpack.packb({
            'emoji': emoji,
            'original_event': {
                'id': original_event_id,
                'room_id': evt.room_id,
                'body': original_event.content.body,
                'formatted_body': original_event.content.formatted_body
            },
            'sender': evt.sender,
            'timestamp': evt.timestamp
        }))

    async def action_loop(self) -> None:
        self.log.debug('action_loop started')
        actions_sub = await self.nats.subscribe(f'{self.config["nats_topic_prefix"]}_action', queue='maubot')
        while True:
            try:
                action_pack = await actions_sub.next_msg()
                action = msgpack.unpackb(action_pack.data)

                result = await operator.attrgetter(action['method'])(self)(**action['kwargs'])
                self.log.debug(result)
            except nats.errors.TimeoutError:
                continue
            except asyncio.CancelledError:
                break
            except Exception as e:
                self.log.exception(e)

    async def stop(self) -> None:
        self.action_task.cancel()
