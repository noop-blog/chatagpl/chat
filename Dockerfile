FROM python:3.11-slim

RUN useradd -m -d /app app

WORKDIR /app

USER 1000
ENV PATH=/app/.local/bin:${PATH}

COPY requirments.txt .

RUN pip3 install --no-cache-dir -r requirments.txt

COPY /src/* .

CMD python3 -m maubot.standalone
